<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Models\Article;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Laravel\Sail\Console\PublishCommand;

Route::get('/', [PublicController::class, 'welcome'])->name("home");

// Rotte articleController
Route::get('/articles/{article}/show', [ArticleController::class, 'show'])->name('show');

Route::get('/articles/{category}/index_category', [ArticleController::class, 'articlesByCategory'])->name('category');

Route::get('/articles/{user}/index_author', [ArticleController::class, 'articlesByAuthor'])->name('author');

// Rotta Lavora con noi
Route::get('/work-with-us', [PublicController::class, 'workWithUs'])->name('work.with.us');
Route::post('/user/send-role-request', [PublicController::class, 'sendRoleRequest'])->name('user.role.request');

// Rotta admin
Route::middleware('admin')->group(function () {
    Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('adminDashboard');
    Route::get('/admin/{user}/set-revisor', [AdminController::class, 'makeUserRevisor'])->name('admin.makeUserRevisor');
    Route::get('/admin/{user}/set-admin', [AdminController::class, 'makeUserAdmin'])->name('admin.makeUserAdmin');
    Route::get('/admin/{user}/set-writer', [AdminController::class, 'makeUserWriter'])->name('admin.makeUserWriter');
    Route::post('/tag/store', [AdminController::class, 'storeTag'])->name('tag.store');
    Route::post('/category/{category}/update', [AdminController::class, 'editCategory'])->name('category.edit');
    Route::delete('category/{category}/delete', [AdminController::class, 'deleteCategory'])->name('category.delete');
    Route::post('/category/store', [AdminController::class, 'storeCategory'])->name('category.store');
});

// Rotte writer
Route::middleware('writer')->group(function () {
    Route::get('/articles/create', [ArticleController::class, 'articlesCreate'])->name("crea");
    Route::post('article/store', [ArticleController::class, 'articleStore'])->name('salva');
    Route::get('article/dashboard', [ArticleController::class, 'articleDashboard'])->name('articlesDashboard');
    Route::get('/article/{article}/edit', [ArticleController::class, 'edit'])->name('articleEdit');
    Route::put('/article/{article}/update', [ArticleController::class, 'articleUpdate'])->name('article.update');
    Route::get('/article/{article}/delete', [ArticleController::class, 'articleDestroy'])->name('article.delete');
});

// Rotte revisor
Route::middleware('revisor')->group(function () {
    Route::get('/revisor/dashboard', [RevisorController::class, 'revisorDashboard'])->name("revisor.dashboard");
    Route::get('/revisor/article/{article}/detail', [RevisorController::class, 'articleDetail'])->name("revisor.detail");
    Route::get('/revisor/article/{article}/accept', [RevisorController::class, 'acceptArticle'])->name("revisor.accept");
    Route::get('/revisor/article/{article}/reject', [RevisorController::class, 'rejectArticle'])->name("revisor.reject");
});

// Rotte search
Route::get('/article/search', [PublicController::class, 'searchArticle'])->name('search.articles');

// Rotte Tag
Route::post('/tag/{tag}/update', [AdminController::class, 'editTag'])->name('tag.edit');
Route::delete('/tag/{tag}/delete', [AdminController::class, 'deleteTag'])->name('tag.delete');
