

// animazione navbar
let prevScrollPos = window.pageYOffset;

window.onscroll = function () {
    let currentScrollPos = window.pageYOffset;
    if (prevScrollPos > currentScrollPos) {
        document.querySelector("nav").style.top = "0";
    } else {
        document.querySelector("nav").style.top = "-100px";
    }

    prevScrollPos = currentScrollPos;
};

// animazione card
const scrollElements = document.querySelectorAll(".card-animation");

const elementInView = (el, dividend = 1) => {
  const elementTop = el.getBoundingClientRect().top;

  return (
    elementTop <=
    (window.innerHeight || document.documentElement.clientHeight) / dividend
  );
};

const elementOutofView = (el) => {
  const elementTop = el.getBoundingClientRect().top;

  return (
    elementTop > (window.innerHeight || document.documentElement.clientHeight)
  );
};

const displayScrollElement = (element) => {
  element.classList.add("scrolled");
};


const handleScrollAnimation = () => {
  scrollElements.forEach((el) => {
    if (elementInView(el, 1.25)) {
      displayScrollElement(el);
    }
  })
}

window.addEventListener("scroll", () => { 
  handleScrollAnimation();
});


//! animazione search bar
const welcome = document.querySelector(".container");

if (welcome.classList.contains("welcome")) {

    const cursor = document.querySelector(".cursor"); // cursore di scrittura
    const elTesto = document.querySelector(".text-anim"); // testo animato
    const animazione = document.querySelector(".placeholder-text"); // testo fisso "cerca per"
    const searchbar = document.querySelector("#typebar"); //input tag
    
    const frasi = ["categoria", "titolo", "sottotitolo"];
    let n = 0;
    let parola = 0;
    let carattere = 0;
    let intervalVal;
    
    // funzione scrivi
    function scrivi() {
    
        let text = frasi[parola].substring(0, carattere + 1); //parte dal carattere 0 e arriva a "carattere + 1"
        // inizia a scrivere
        elTesto.innerHTML = text;
        // incrementa per prendere successiva lettera della parola
        carattere++;
    
        // funzione è ripetuta finché la parola non è completa
        // se la parola è completa, inizia a cancellare
        if (text === frasi[parola]) {
            // blocca il loop per scrivere
            clearInterval(intervalVal);
            // avvia il loop per cancellare
            setTimeout(function () {
                intervalVal = setInterval(cancella, 90);
            }, 1100);
        }
    }
    
    function cancella() {
        var text = frasi[parola].substring(0, carattere - 1);
        elTesto.innerHTML = text;
        carattere--;
    
        if (text === '') {
            clearInterval(intervalVal);
    
            if (parola == (frasi.length - 1))
                parola = 0;
            else
                parola++;
    
            carattere = 0;
    
            setTimeout(function () {
                intervalVal = setInterval(scrivi, 120);
            }, 1000);
        }
    }
    
    // cursor blink
    setInterval(function () {
        if (n === 0) {
            cursor.innerHTML = "|";
            n = 1;
        } else {
            cursor.innerHTML = "";
            n = 0;
        };
    }, 500);
    
    // avvia animazione scrittura
    intervalVal = setInterval(() => {
        scrivi();
    }, 120);
    
    searchbar.addEventListener("focus", () => {
        animazione.style.display = "none";
    });
    
    animazione.addEventListener("click", () => {
        animazione.style.display = "none";
        searchbar.focus();
    });
    
    searchbar.addEventListener("focusout", () => {
        if (searchbar.value.length == 0) {
            animazione.style.display = "inline";
        } else {
            animazione.style.display = "none";
        }
    })
}



