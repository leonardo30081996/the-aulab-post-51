<x-layout>
    
    <div class="container welcome-margin my-auto">
        <div class="row">
            
            <div class="col-lg-5">
                <div class="container PositionContainer position-relative">
                    <div class="row">
                        <div class="col-12 text-center p-4">
                            <h2 class="my-4">Registrati</h2>
                            {{-- Inizio form registrazione --}}
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="mb-3 text-start">
                                    <label for="exampleInputEmail1" class="form-label fs-5">Inserisce il tuo Nome</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name">
                                </div>
                                <div class="mb-3 text-start">
                                    <label for="exampleInputEmail1" class="form-label fs-5">Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                                </div>
                                <div class="mb-3 text-start">
                                    <label for="exampleInputPassword1" class="form-label fs-5">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                                </div>
                                <div class="mb-3 mb-4 text-start">
                                    <label for="exampleInputPassword1" class="form-label fs-5"> Conferma Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" name="password_confirmation">
                                </div>
                                <button type="submit" class="btn btn-custom fw-semibold">Registrati</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-7">
                <div class="imgCustomRegister"></div>
            </div>
        </div>
    </div>
</x-layout>
