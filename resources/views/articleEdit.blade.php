<x-layout>
    <div class="container PositionContainer welcome-margin my-5 border border-1 border-dark">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('article.update', compact('article')) }}" method="post"
                    enctype="multipart/form-data" class="p-5 text-center">
                    @csrf
                    @method('put')

                    <div class="mb-3">
                        <label for="title" class="form-label">Titolo:</label>
                        <input name="title" type="text" class="form-control" id="title"
                            value="{{ $article->title }}">
                    </div>

                    <div class="mb-3">
                        <label for="subtitle" class="form-label">Sottotitolo:</label>
                        <input name="subtitle" type="text" class="form-control" id="subtitle"
                            value="{{ $article->subtitle }}">
                    </div>

                    <div class="mb-3">
                        <label for="category" class="form-label">Categoria :</label>
                        <select class="form-control" name="category_id">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{ $category->id == $article->category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="mb-3">
                        <label for="tags" class="form-label">Tags :</label>
                        <select name="tags[]" class="form-control" multiple>
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->id }}"
                                    {{ $article->tags->contains($tag) ? 'selected' : '' }}>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="my-3">
                        <label for="" class="form-label">Copertina attuale:</label> <br>
                        <div class="text-center">
                            <img class="img-fluid rounded-2" src="{{ Storage::url($article->img) }}"
                                alt="{{ Storage::url($article->img) }}">
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="image" class="form-label">Copertina:</label>
                        <input name="img" type="file" class="form-control" id="image">
                    </div>

                    <div class="mb-3">
                        <label for="body" class="form-label">Corpo del testo:</label>
                        <textarea name="body" id="body" cols="30" rows="6" class="form-control">{{ $article->description }}</textarea>
                    </div>

                    <div class="mt-2">
                        <button class="btn btn-custom">Pubblica articolo</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</x-layout>
