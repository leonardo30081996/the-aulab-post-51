<x-layout>
    <div class="container PositionContainer welcome-margin  my-auto">
        <div class="row mt-5">
            <h2 class="text-center">{{$category->name}}</h2>
            @foreach ($articles as $article)
                {{-- div immagine --}}
                <div class="col-12 col-lg-7 p-4 text-center overflow-hidden">
                    <div class="col-12 divsp">
                        <img class="img-fluid rounded-2 custom-img mb-4" src="{{ Storage::url($article->img) }}"
                            alt="{{ Storage::url($article->title) }}">
                        <div class="bg-CustomCardP2"></div>
                    </div>
                </div>
                {{-- div dettagli --}}
                <div class="col-12 col-lg-5 text-center pt-3">
                    <h5 class="fs-2 fw-bolder text-center my-4">{{ $article->title }}</h5>
                    <h5 class="fs-3 text-center">{{ substr($article->subtitle, 0, 20) }}</h5>
                    <a class="my-4" href="{{ route('category', $article->category) }}">{{ $article->category->name }}</a>
                    <br>
                    <a class="btn btn-custom my-4"
                        href="{{ route('show', $article) }}">Maggiori Info</a>
                </div>
            @endforeach
        </div>
    </div>
</x-layout>
