<x-layout>
    <div class="container PositionContainer position-relative welcome-margin mx-auto my-auto border border-1 border-dark">
        
            <div class="row my-5 p-4">
                <div class="col-12 text-center">
                    <h1>Bentornato, {{Auth::user()->name}}</h1>
                </div>
            </div>
        
    
        
            <div class="row my-5 p-4">
                @if(count(Auth::user()->articles)>0)
                
                <div class="col-12 text-center">
                    <h2>Tutti i tuoi articoli</h2>
                </div>

                <div class="col-12 my-3">
                    <x-articles-table :articles="Auth::user()->articles"/>
                </div>
                @else
                <div class="col-12 my-3">
                    <h2>Non hai scritto alcun articolo</h2>
                    <a href="{{route('crea')}}" class="btn btn-custom">Crea il tuo primo articolo</a>
                </div>
                @endif
            </div>
        
    </div>
    <div class="imgCustomOpzioniScrittore"></div>
    <div class="imgCustomOpzioniScrittore2"></div>
</x-layout>