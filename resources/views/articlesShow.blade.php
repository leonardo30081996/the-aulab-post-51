<x-layout>
    <div class="container PositionContainer welcome-margin p-5 my-auto">
        <div class="row">
            <h1 class="fw-bold mt-3 mb-5 fs-2 text-center">Dettagli articolo</h1>
            <div class="col-lg-5 mt-4 px-4">
                <div class="col-12 col-md-6">
                    <h2>Titolo:</h2>
                    <h1>{{ $article->title }}</h1>
                    <h2>Sottotitolo:</h2>
                    <h3>{{ $article->subtitle }}</h3>
                </div>
                
            </div>
            <div class="col-lg-6 px-4">
                <img class="rounded-2 my-2 custom-img" src="{{ Storage::url($article->img) }}"
                    alt="{{ Storage::url($article->title) }}">
            </div>
            <div class="col-12 px-4">
                <p class="fs-4">{{ $article->description }}</p>
                <p>
                    <a href="{{ route('category', $article->category) }}" class="fs-4 fw-bold">
                        {{ $article->category->name }}
                    </a>
                </p>
                <p class="fw-bolder">Pubblicato da: {{ $article->user->name }}</p>
                <p class="fw-bolder">Pubblicato il: {{ $article->created_at->format('d/m/Y') }}</p>
                <div class="d-flex">
                    <p class=" fw-bolder fs-3">Tag : </p>
                    @foreach ($article->tags as $tag)
                        <span class=" fw-bolder fs-3"> #{{ $tag->name }}</span>
                    @endforeach
                </div>
            </div>
        </div>


    </div>

</x-layout>
