<x-layout>
    <div class="container PositionContainer welcome-margin my-auto ">
        <div class="row justify-content-between ">
            <div class="col-12  ">
                <h1 class="text-center">Articoli per: {{ $key }}</h1>
            </div>
      
           

    
        
            @foreach ($articles as $article)
                <div class="col-12 col-lg-5 my-4 mx-auto ">
                    <div >
                        <img src="{{Storage::url($article->img)}}" alt="{{$article->title}}" class="img-top img-fluid rounded-5">
                    </div>    
                </div>
                <div class="col-12 col-lg-6 my-4">        
                    <div class="card-body">
                        <h5 class="title">{{$article->title}}</h5>
                        <p class="text fs-4">{{$article->subtitle}}</p>
                        <a href="{{route('show', compact('article'))}}" class="btn btn-custom mt-2 ">Leggi</a>
                    </div>
                </div>
            @endforeach
        </div>
        
    </div>
</x-layout>
