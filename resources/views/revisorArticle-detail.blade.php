<x-layout>
    <div class="container PositionContainer welcome-margin my-auto p-4 border border-1 border-dark">
        <div class="row">
            <div class="col-12 col-md-6 ">
                <h1 class="text-center mt-3">Revisione articolo</h1>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-12 col-md-6 mb-5 me-5">
                <img src="{{Storage::url($article->img)}}" alt="" class="img-fluid rounded-2">
            </div>

            <div class="col-12 col-md-5">
                <h3 class="my-3 fs-3"><span class="fw-bold">Titolo: </span>{{$article->title}}</h3>
                <h3 class="my-3 fs-3"><span class="fw-bold">Descrizione: </span>{{$article->subtitle}}</h3>
                <h3 class="my-3 fs-3"><span class="fw-bold">Autore: </span>{{$article->user->name}}</h3>
                <h3 class="my-3 fs-3"><span class="fw-bold">Categoria: </span>{{$article->category->name}}</h3>
                <h3 class="my-3 fs-3"><span class="fw-bold">Creato: </span>{{$article->created_at->diffForHumans()}}</h3>
                {{-- bottoni accetta-rifiuta --}}
                <div class="my-5">
                    <a href="{{route('revisor.accept', $article)}}" class="btn btn-custom ms-2 me-4">Accetta</a>
                    <a href="{{route('revisor.reject', $article)}}" class="btn btn-custom">Rifiuta</a>
                </div>

            </div>
        </div>
    </div>
</x-layout>