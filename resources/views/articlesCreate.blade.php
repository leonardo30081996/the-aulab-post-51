<x-layout>
    <div class="container PositionContainer position-relative  welcome-margin p-4 my-5 border border-1 border-dark">
        <div class="row col-12 text-center mx-auto mt-3">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="" method="POST" action="{{ route('salva') }}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label fw-semibold fs-5 mb-4">Titolo</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="title">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label  fw-semibold fs-5  mb-4">Sottotitolo</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" name="subtitle">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label  fw-semibold fs-5  mb-4">Categoria</label>
                    <select type="text" class="form-control" id="exampleInputPassword1" name="category_id">
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <x-form :tags="$tags" />
                <div class="mb-3">
                    <label class="form-label  fw-semibold fs-5  mb-4">Descrizione</label>
                    <textarea class="form-control" name="description"></textarea>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label  fw-semibold fs-5  mb-4">Inserisci la tua
                        immagine
                    </label>
                    <input type="file" class="form-control" id="exampleInputPassword1" name="img">
                </div>
                <button type="submit" class="btn btn-custom mb-4">Invia</button>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </form>
        </div>
    </div>

    <div class="imgCustomOpzioniScrittore1"></div>
    <div class="imgCustomOpzioniScrittore2b"></div>
</x-layout>
