<x-layout>
    <div class="container PositionContainer welcome-margin mx-auto my-auto border border-1 border-dark">
        <div class="row my-5">
            <div class="col-12">
                <h2 class="ms-5 text-center">Bentornato {{Auth::user()->name}}</h2>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12">
                <x-revisor-table :articles="$articles"/>
            </div>
        </div>
    </div>
</x-layout>