<x-layout>
    <div class="container PositionContainer welcome-margin border border-1 border-dark">
        <div class="row mx-2">
            <h1 class="my-5 text-center">Lavora con noi!</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="container">
                <div class="row mb-5">
                    <div class="col-12 col-md-6 mb-5 ">
                        <h3 class="text-center">Lavora come Scrittore</h3>
                        <p>Se desideri accedere alla posizione dello scrittore, avendo così la possibilità di scrivere nuovi articoli compila il form ed un admin valuterà la tua candidatura.</p>
                        <h3 class="text-center">Lavora come Revisore</h3>
                        <p>Se desideri accedere alla posizione di revisore, avendo così la possibilità di accettare o rifiutare gli articoli, valutandone il loro contenuto, compila il form.</p>
                        <h3 class="text-center">Lavora come Amministratore</h3>
                        <p>Se desideri accedere alla posizione dell'admin, avendo così la possibilità di accettare le richieste degli utenti, revisionare o scrivere un articolo, amministrando i contenuti della nostra pagina, compila il form.</p>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <form method="POST" action="{{ route('user.role.request') }}">
                            @csrf
                            <div class="mb-3">
                                <label class="form-label fw-semibold fs-5">Per quale posizione desideri candidarti?</label>
                                <select class="form-control" name="role">
                                    <option value="admin">
                                        Amministratore
                                    </option>
                                    <option value="revisor">
                                        Revisore
                                    </option>
                                    <option value="writer">
                                        Scrittore
                                    </option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="form-label fw-semibold fs-5">Inviaci la tua E-mail</label>
                                <input type="email" class="form-control" name="email" @auth
                                    value="{{ Auth::user()->email }}" @endauth>
                            </div>
                            <div class="mb-3">
                                <label class="form-label fw-semibold fs-5">Perchè dovremmo assumerti?</label>
                                <textarea class="form-control" name="presentation" id="" cols="30" rows="10"></textarea>
                            </div>
                            <button type="submit" class="btn btn-custom ">Invia Candidatura</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-layout>
