<x-layout>
    <div class="container PositionContainerAdmin welcome-margin p-5 my-auto">
        <h1 class="fw-bolder text-center p-4 my-3">Area riservata agli admin</h1>
        <p class="text-center">Gestisci richieste, crea e modifica tag e categorie</p>
        <div class="row">
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item border-0">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button fs-2 collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Richieste
                        </button>
                    </h2>

                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            <div class="container">
                                <div class="row my-5">
                                    <div class="col-12">
                                        <h2 class="text-center">Richieste di amministratore</h2>
                                        <x-admin-requests-table :adminRequests="$adminRequests" />
                                    </div>
                                </div>
                            </div>
                            {{-- richieste revisori --}}
                            <div class="container my-3">
                                <div class="row my-5">
                                    <div class="col-12">
                                        <h2 class="text-center">Richieste di revisore</h2>
                                        <x-revisor-requests-table :revisorRequests="$revisorRequests" />
                                    </div>
                                </div>
                            </div>
                            {{-- richieste autori --}}
                            <div class="container my-3">
                                <div class="row my-5">
                                    <div class="col-12">
                                        <h2 class="text-center">Richieste di scrittore</h2>
                                        <x-writer-requests-table :writerRequests="$writerRequests" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item border-0">
                    <h2 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button fs-2 collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Tags
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-12 my-2">
                                    <h2>Crea Tag #</h2>
                                    <x-tags-form />
                                </div>
                                <div class="col-12 mb-5">
                                    <h2>Gestisci i Tag #</h2>
                                    <x-tags-table :tags="$tags" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item border-0">
                    <h2 class="accordion-header" id="flush-headingThree">
                        <button class="accordion-button fs-2 collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseThree" aria-expanded="false"
                            aria-controls="flush-collapseThree">
                            Categorie
                        </button>
                    </h2>
                    <div id="flush-collapseThree" class="accordion-collapse collapse"
                        aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-12 mb-4">
                                    <h2>Crea categoria</h2>
                                    <x-categories-form />
                                </div>
                                <div class="col-12">
                                    <h2>Gestisci categorie</h2>
                                    <x-categories-table :categories="$categories" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>
