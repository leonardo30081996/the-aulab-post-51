<x-layout>
    <div class="container PositionContainer-smartphone welcome-margin p-0 welcome">
        <div class="ColorSfond1"></div>
        <div class="ColorSfond2"></div>
        @if (session('message'))
            <div class="alert alert-Custom mt-4 mx-5 fs-3 fw-bolder text-center">
                {{ session('message') }}
            </div>
        @endif

        <div class="row my-5 mx-auto justify-content-around">

            <div id="box-smartphone" class="box mx-4">
                <form action="{{ route('search.articles') }}" method="GET" name="search" autocomplete="off">
                    <div class="searchbar">
                        <input id="typebar" type="text" name="key" class="input" onmouseout="this.value = ''; this.blur();">
                        <span class="placeholder-text">
                            <span class="fixed-text">Cerca per</span>
                            <div class="text-anim"></div><span class="cursor"></span>
                        </span>
                        <button class="btn hide" type="submit"></button>
                    </div>
                </form>
                <i class="bi bi-search"></i>
            </div>

            <!-- categorie -->
            <div
                class="col-lg-3 border border-1 border-dark p-3 text-center d-flex flex-column justify-content-center card-style-categorie ">
                <div class="col-12 p-3">
                    <h3>Categorie</h3>
                </div>
                <div class="col-12">
                    <div class="dropdown">
                        <button class="btn btn-custom w-100" type="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Clicca qui
                        </button>
                        <ul class="dropdown-menu w-100">
                            @foreach ($categories as $category)
                                <li>
                                    <a href="{{ route('category', $category) }}" class="btn-CustomStatic4 mx-3 mt-3"
                                        type="button">
                                        {{ $category->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- card politica -->
            <div class="col-lg-8 border border-1 border-dark p-3 card-style">
                <div class="col-12">
                    <img class="w-100 img-f1" src="/images/politica.jpg" alt="">
                </div>
                <div class="col-12">
                    <p>L'uomo si distrugge con la politica senza princìpi, col piacere senza la coscienza, con la
                        ricchezza senza lavoro, con la conoscenza senza carattere, con gli affari senza morale, con la
                        scienza senza umanità, con la fede senza sacrifici. </p>
                    <a href="{{ route('category', $categories[0]) }}"
                        class="btn btn-custom float-end">{{ $categories[0]->name }}</a>
                </div>
            </div>
        </div>

        <div class="row justify-content-around mx-auto mb-5">
            <div class="col-lg-7">
                <div class="col-12 border border-1 border-dark p-3 card-style">
                    <div class="col-12">
                        <p>Negli ultimi 50 anni a causa dei cambiamenti climatici dovuti alle attività antropiche, il
                            cibo, l'intrattenimento e la natura sono cambiate moltissimo... se vuoi saperne di più
                            clicca qui.</p>

                        <div class="col-12 d-flex justify-content-around">
                            <a href="{{ route('category', $categories[2]) }}"
                                class="btn btn-custom">{{ $categories[2]->name }}</a>
                            <a href="{{ route('category', $categories[4]) }}"
                                class="btn btn-custom">{{ $categories[4]->name }}</a>
                            <a href="{{ route('category', $categories[0]) }}"
                                class="btn btn-custom hide-smart">{{ $categories[0]->name }}</a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-12 d-flex justify-content-between">
                    <div
                        class="col-5 border border-1 border-dark p-3 mt-5 d-flex flex-column justify-content-between card-style">
                        <p>Scopri le ultime notizie riguardanti i temi del Tech. dalla nascita del metaverso fino alle
                            ultime scoperte spaziali, interviste esclusive con i migliori scienziati del settore.</p>
                        <div>
                            <a href="{{ route('category', $categories[5]) }}"
                                class="btn btn-custom float-end">{{ $categories[5]->name }}</a>
                        </div>
                    </div>
                    <div
                        class="col-6 border border-1 border-dark p-3 mt-5 d-flex flex-column justify-content-between card-style">
                        <p>Scopri le ultime notizie del settore del calcio, tennis, caccia, pesca, padding, mma, muay
                            thai, kick-boxing, boxe, judo, tae kwon do, jeet kune do, wing chun e ping pong. clicca qui
                        </p>
                        <div>
                            <a href="{{ route('category', $categories[3]) }}"
                                class="btn btn-custom">{{ $categories[3]->name }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card economia -->
            <div class="col-lg-4 border border-1 border-dark p-3 d-flex flex-column justify-content-between card-style">
                <div class="col-12">
                    <img class="w-100 img-f2" src="/images/economia.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="">
                        <p>Tutte le ultime notizie riguardanti l'economia italiana, la borsa con l'avvento draghi
                            sembrerebbe stabile, per scoprirne di più clicca qui.</p>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="{{ route('category', $categories[1]) }}"
                        class="btn btn-custom">{{ $categories[1]->name }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container position-relative my-auto Container2Posizione">
        <h2 class="text-center my-5 TitleCustom ">Le ultime notizie dal mondo</h2>
        <img class="position-absolute img-reading" src="my-img/reading.svg" alt="">
        <div class="row">
            @foreach ($articles as $article)
                <div class="col-12 col-lg-5 mx-auto my-5  ">
                    <div class="card2 overflow-hidden card-animation slide-left ">
                        <div class="row g-0">
                            <div class="col-6 col-lg-4 cardCustom">
                                <img class="imgCustom1" src="{{ Storage::url($article->img) }}"
                                    alt="{{ Storage::url($article->title) }}">
                            </div>

                            <div class="col-6 col-lg-8">
                                <div class="text-center pt-3">

                                    {{-- categoria --}}
                                    <a class="categoryLink fs-4 aCard"
                                        href="{{ route('category', $article->category) }}">{{ $article->category->name }}
                                    </a>
                                    <br>
                                    {{-- autore --}}
                                    <a class="fs-4 aCard" href="{{ route('author', $article->user) }}">By:
                                        {{ $article->user->name }}</a>
                                    {{-- titolo --}}
                                    <h5 class="fs-3 fw-bolder text-center aCard">{{ $article->title }}
                                    </h5>
                                    {{-- sottotitolo --}}
                                    <h5 class="fs-4 text-center aCard">{{ substr($article->subtitle, 0, 20) }}
                                    </h5>
                                    {{-- button --}}
                                    <a class="btn btn-custom my-2 text-center text-dark my-3"
                                        href="{{ route('show', $article) }}">Maggiori info
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</x-layout>
