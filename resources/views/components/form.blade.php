<div class="mb-3">
    <label class="form-label fw-semibold fs-5">Tags</label>
    <select name="tags[]" class="form-control" multiple>
        @foreach ($tags as $tag)
            
            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach

    </select>
</div>
