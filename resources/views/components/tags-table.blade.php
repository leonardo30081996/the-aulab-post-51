<div class="container">
    <div class="row">
        @foreach ($tags as $tag)
            <div class="col-12 my-4">
                <div class="row">
                    <div class="col-lg-5">
                        <h4 class="fw-bolder">#</h4>
                    </div>
                    <div class="col-lg-5">
                        <h4>{{ $tag->id }}</h4>
                    </div>
                    <div class="col-lg-5">
                        <h4 class="fw-bolder">Tag</h4>
                    </div>
                    <div class="col-lg-5">
                        <h4>{{ $tag->name }}</h4>
                    </div>
                    <div class="col-lg-5">
                        <h4 class="fw-bolder">Articoli</h4>
                    </div>
                    <div class="col-lg-5">
                        <h4>{{ count($tag->articles) }}</h4>
                    </div>
                    <div class="col-lg-5 d-flex flex-column justify-content-center">
                        <h4 class="fw-bolder">Modifica</h4>
                    </div>
                    <div class="col-lg-6">
                        <form action="{{ route('tag.edit', $tag) }}" method="POST">@csrf
                            <input type="text" class="form-control col-lg-6" placeholder="Nuovo nome "name="name" required>
                            <button class="btn btn-custom my-3 col-lg-5" type="submit">Salva</button>
                        </form>
                    </div>
                    <div class="col-lg-5 d-flex flex-column justify-content-center">
                        <h4 class="fw-bolder">Elimina</h4>
                    </div>
                    <div class="col-lg-6">
                        <form class="" action="{{ route('tag.delete', $tag) }}" method="POST">@csrf
                            @method('DELETE')
                            <button class="btn btn-custom mt-2" type="submit">Fuori dalle scatole </button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>


</div>
