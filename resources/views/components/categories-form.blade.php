

<form action="{{ route('category.store') }}" method="POST">
    @csrf
    <label><p>Inserisci una nuova categoria</p></label>
    <div class="col-12">
        <input type="text" class="form-control w-50 ms-5 mt-3" placeholder="nome tag" name='name' required>
        <button class="btn btn-custom my-3  ms-5 " type="submit">Salva</button>
    </div>
</form>