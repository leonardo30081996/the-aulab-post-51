{{-- <table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">email</th>
            <th scope="col">action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($adminRequests as $user)
        <tr>
            <th scope="row">{{$user->id}}</th>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>
                <a href="{{route('admin.makeUserAdmin', $user)}}" class="btn btn-primary">Rendi writer
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table> --}}
<div class="container">

    <div class="row mt-4 ms-2">
        <div class="col-5">
            <div class="col-3"><h4>#</h4></div>
            <div class="col-3"><h4>Nome</h4></div>
            <div class="col-3"><h4>Email</h4></div>
            <div class="col-3"><h4>Azione</h4></div>
        </div>
    
        <div class="col-6">
            @foreach ($adminRequests as $user)
            <div class="col-3"><h4>{{$user->id}}</h4></div>
            <div class="col-3"><h4>{{$user->name}}</h4></div>
            <div class="col-3"><h4>{{$user->email}}</h4></div>
            <div class="col-12 mb-3">
                <a class="btn btn-custom" href="{{route('admin.makeUserAdmin', $user)}}">Rendi admin</a>
            </div>
            @endforeach
        </div>
    </div>
</div>