
<div class="container">
    <div class="row">
        @foreach ($articles as $article)
            <div class="col-12 my-4">
                <div class="row justify-content-center text-end">
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">#</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->id }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Titolo</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->title }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Creato il:</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->created_at->format('d/m/y') }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Status</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->is_accepted ? 'Pubblicato' : 'Non pubblicato' }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Modifica</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <a href="{{ route('articleEdit', $article) }}" class="btn btn-custom my-3">Modifica</a>
                    </div>

                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Elimina</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <form action="{{ route('article.delete', $article) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-custom my-3" type="submit">Elimina</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
