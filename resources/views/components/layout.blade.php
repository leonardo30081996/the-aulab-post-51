<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AulabPost</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    {{-- Bree Serif, Raleway font --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Bree+Serif&family=Raleway:ital,wght@0,400;0,500;0,600;0,700;1,400&display=swap"
        rel="stylesheet">


</head>

<body class="d-flex flex-column min-vh-100 ">
    <x-nav></x-nav>
    {{ $slot }}
    <x-footer></x-footer>

    {{-- side menu desktop --}}
    <div class="offcanvas offcanvas-end bg-OpzionR  fs-3 fw-semibold" tabindex="1" id="offcanvasRight"
        aria-labelledby="offcanvasRightLabel">
        
        <div class="offcanvas-body">
            @auth
            <a class="fs-3 user-name">Benvenuto/a &nbsp; {{Auth::user()->name}}</a>
            @endauth
            <a class="nav-link my-3 " href="{{ route('crea') }}">Inserisci Articolo</a>
            <a class="nav-link my-3" href="{{ route('adminDashboard') }}">Area riservata</a>
            <a class="nav-link my-3 " href="{{ route('revisor.dashboard') }}">Area revisori</a>
            @if (Auth::user() && Auth::user()->is_writer)
                <a class="nav-link my-3 " href="{{ route('articlesDashboard') }}">Area
                    scrittori</a>
            @endif
            <a class="nav-link my-3" href="{{ route('work.with.us') }}">Lavora con noi</a>


            <a class="nav-link my-3" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>

            <div class="my-5 text-center">
                <img class="rounded-circle img-logo-2" src="/my-img/logo2.png" alt="logo-2">
            </div>
        </div>
    </div>
</body>

</html>
