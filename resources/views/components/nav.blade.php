{{-- navbar Custom --}}
<nav class="navbar navbar-expand-md bg-ColorNavbarCustom mb-5 border border-2 border-top-0 border-start-0 border-end-0 border-dark w-100"
    id="navbar">
    <div class="container-fluid">
        <a href="{{ route('home') }}" class="ms-2">
            <img src="/my-img/logoPost2.png" width="75px" alt="logo">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavDropdown">

            <ul class="navbar-nav ms-auto text-center">
                @guest
                <div class="iconNavbar mx-3">
                    <a href="{{route('home')}}"><i class="bi bi-house "></i></a>
                </div>

                <div class="box mx-3">
                    <form action="{{ route('search.articles') }}" method="GET" name="search">
                        <input type="text" name="key" class="input"
                        onmouseout="this.value = ''; this.blur();">
                        <button class="btn hide" type="submit"></button>
                    </form>
                    <i class="bi bi-search"></i>
                </div>

                    <li class="nav-item">
                        <a class="btn btn-custom-navbar nav-link mx-auto textColorCustom"
                            href="{{ route('register') }}">Registrati</a>
                        
                            <a class="btn btn-custom-nav-smt nav-link mx-3 textColorCustom"
                            href="{{ route('register') }}"><i class="bi bi-lock"></i></a>
                    </li>
                    <li class="nav-item ">
                        <a class="btn btn-custom-navbar nav-link mx-auto textColorCustom"
                            href="{{ route('login') }}">Accedi</a>

                        <a class="btn btn-custom-nav-smt nav-link mx-3 textColorCustom"
                            href="{{ route('login') }}"><i class="bi bi-key"></i></a>
                    </li>
                @else
                    <div class="smartScreen ">
                        <a class="nav-link my-3  textColorCustom fs-3 " href="{{ route('crea') }}">Inserisci Articolo</a>
                        <a class="nav-link my-3  textColorCustom fs-3" href="{{ route('adminDashboard') }}">Area
                            riservata</a>
                        <a class="nav-link my-3  textColorCustom fs-3 " href="{{ route('revisor.dashboard') }}">Area
                            revisori</a>


                        @if (Auth::user() && Auth::user()->is_writer)
                            <li class="nav-item">
                                <a class="nav-link my-3  textColorCustom fs-3 " href="{{ route('articlesDashboard') }}">Area
                                    scrittori</a>
                            </li>
                        @endif

                        <a class="nav-link my-3  textColorCustom fs-3 " href="{{ route('work.with.us') }}">Lavora con
                            noi</a>
                        <a class="nav-link my-3  textColorCustom fs-3" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    </div>
                    
                    {{-- tasto home --}}
                    <div class="iconNavbar">
                        <a href="{{route('home')}}"><i class="bi bi-house "></i></a>
                    </div>
                    {{-- searchbar --}}
                    <div class="box mx-4">
                        <form action="{{ route('search.articles') }}" method="GET" name="search">
                            <input type="text" name="key" class="input"
                            onmouseout="this.value = ''; this.blur();">
                            <button class="btn hide" type="submit"></button>
                        </form>
                        <i class="bi bi-search"></i>
                    </div>
                    {{-- tasto profilo --}}
                    <button class=" btn-NavbarCustom buttonMenu me-3" type="button" data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                        <i class="bi bi-person-circle"></i>
                    </button>
                @endguest

            </ul>
        </div>

    </div>
</nav>
