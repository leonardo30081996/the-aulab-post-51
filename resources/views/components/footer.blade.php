<footer class="border border-2 border-light border-bottom-0 border-end-0 border-start-0 rounded-3 mt-5">
    <div class="container-fluid ContainerCustomFooter border border-bottom-0 border-end-0 border-start-0 rounded-3">
        <div class="row">
            <div class="col-lg-3 d-flex justify-content-center align-items-center ">
                <img class="rounded-circle img-logo-2" src="/my-img/logo2.png" alt="logo-2">
            </div>
            <div class="col-lg-3 p-5 text-light">
                <h4 class="mb-3">Seguici</h4>
                <ul class="p-0">
                    <li>
                        <a class="text-light fs-5 d-flex" href="#">
                            <i class="bi bi-envelope fs-3 me-2"></i>
                            <p>aulabpost@aulab.com</p>
                        </a>
                    </li>
                    <li>
                        <a class="text-light fs-5 d-flex" href="#">
                            <i class="bi bi-instagram fs-3 me-2"></i>
                            <p>#aulabpost</p>
                        </a>
                    </li>
                    <li>
                        <a class="text-light fs-5 d-flex" href="#">
                            <i class="bi bi-facebook fs-3 me-2"></i>
                            <p>facebook.it/aulabpost</p>
                        </a>
                    </li>
                    <li>
                        <a class="text-light fs-5 d-flex" href="#">
                            <i class="bi bi-linkedin fs-3 me-2"></i>
                            <p>linkedin.it/aulabpost</p>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 p-5 text-light">
                <h4>Contattaci</h4>
                <ul>
                    <li class="my-2 fs-4">Telefono: +39 381 273 6762</li>
                    <li class="my-2"><a class="fs-4 text-light" href="">Indirizzo: Via Giovanni Amendola 205/3 CA 70025</a></li>
                    <li class="my-2 fs-4">P.IVA: 86334519757</li>
                    <li class="my-2 fs-4"></li>
                </ul>
            </div>

            <div class="col-lg-3 d-flex justify-content-center align-items-center">
                <div class="row">
                    <div class="col-6 text-center">
                        <img class="p-1 border border-2 rounded-circle" src="/my-img/admin.png" alt="admin" width="100px">
                        <h6 class="text-light">Admin</h6>
                    </div>
                    <div class="col-6 text-center">
                        <img class="p-1 border border-2 rounded-circle" src="/my-img/revisor.png" alt="revisor" width="100px">
                        <h6 class="text-light">Revisore</h6>
                    </div>
                    <div class="col-12 text-center">
                        <img class="p-1 border border-2 rounded-circle" src="/my-img/writer.png" alt="writer" width="100px">
                        <h6 class="text-light">Scrittore</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
