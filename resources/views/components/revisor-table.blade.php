<div class="container">
    <div class="row">
        @foreach ($articles as $article)
            <div class="col-12 my-4">
                <div class="row justify-content-center text-end">
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">#</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->id }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Titolo</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->title }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Categorie</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->category->name }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Scritto da:</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->user->name }}</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4 class="fw-bolder text-start">Scritto il:</h4>
                    </div>
                    <div
                        class="col-lg-5 border border-2 border-dark border-start-0 border-top-0 border-bottom-1 border-end-0">
                        <h4>{{ $article->created_at->format('d-m-Y') }}</h4>
                    </div>
                    <div class="col-lg-12 text-center mt-4">
                        <a href="{{ route('revisor.detail', $article) }}"class="btn btn-custom ">Leggi</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
