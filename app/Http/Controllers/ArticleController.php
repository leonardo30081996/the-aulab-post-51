<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\User;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function articlesCreate()
    {

        $tags = Tag::all();
        return view('articlesCreate', compact('tags'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function articleStore(Request $request)
    {
        
        $article = Auth::user()->articles()->create([
            'title'=>$request->input('title'),
            'subtitle'=>$request->input('subtitle'),
            'description'=>$request->input('description'),
            'img'=>$request->file('img')->store("public/img"),
            'category_id'=>$request->input('category_id'),
        
            
        ]);
            
        $selectedTags = $request->input('tags');

        foreach($selectedTags as $tagId){
            $article->tags()->attach($tagId);

        }
        
        return redirect()->route('home')->with("message","L'articolo è stato creato correttamente ed è in attesa di conferma da parte di un revisore.");
    }

    public function articlesByCategory(Category $category)
    {
        $articles = Article::where('category_id', $category->id)->where('is_accepted', true)->orderBy('created_at','DESC')->get();
        return view('articlesCategory',compact('articles', 'category'));
    }

    public function articlesByAuthor(User $user)
    {
        $articles = Article::where('user_id', $user->id)->orderBy('created_at','DESC')->get();
        return view('articlesAuthor', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('articlesShow', compact('article'));
    }


    // Funzione Area riservata Writer

    public function articleDashboard()
    {
        return view('articlesDashboard');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $tags = Tag::all();
        return view('articleEdit', compact('article', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function articleupdate(Request $request, Article $article)
    {
        if($request->has('img')){
            $article->update(
                [
                    'title'=>$request->input('title'),
                    'subtitle'=>$request->input('subtitle'),
                    'description'=>$request->input('description'),
                    'img'=>$request->file('img')->store("public/img"),
                    'category_id'=>$request->input('category_id')

                ]
            );
        } else {
            $article->update(
                [
                    'title'=>$request->input('title'),
                    'subtitle'=>$request->input('subtitle'),
                    'description'=>$request->input('description'),
                    'category_id'=>$request->input('category_id')
                
                ]
                );
        }
        $article->is_accepted=false;
        $article->save();
        $article->tags()->detach();
        $article->tags()->sync($request->input('tags'));
        return redirect()->route('articlesDashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function articleDestroy(Article $article)
    {
        $article->delete();
        return redirect()->route('articlesDashboard');
    }
}
