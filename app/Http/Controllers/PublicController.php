<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Mail\RequestRoleMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;


class PublicController extends Controller
{
    function welcome()
    {
        $articles=Article::where('is_accepted', true)->orderBy('created_at', 'desc')->take(6)->get();
        return view('welcome', compact('articles'));
    }

    // controller rotta lavora con noi
    public function workWithUs()
    {
        return view('workWithUs');
    }

    public function sendRoleRequest(Request $request)
    {
        $user = Auth::user();
        $role = $request->input('role');
        $email = $request->input('email');
        $presentation = $request->input('presentation');
        $requestMail = new RequestRoleMail(compact('role', 'email', 'presentation'));
        Mail::to('admin@blog.it')->send($requestMail);
        switch ($role) {
            case 'admin':
                $user->is_admin = 1;
                break;
            case 'revisor':
                $user->is_revisor = null;
                break;
            case 'writer':
                $user->is_writer = null;
                break;
        }
        $user->update();
        return redirect()->route('home')->with('message', 'Grazie per averci disturbato😋');
    }

    public function searchArticle(Request $request)
    {
        $key = $request->input('key');
        $articles = Article::search($key)->where('is_accepted', true)->get();

        return view('articlesIndex', compact('articles', 'key'));
    }
}


