<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Tag;

class AdminController extends Controller
{
    public function dashboard()
    {
        $adminRequests = User::where('is_admin', NULL)->get();
        $revisorRequests = User::where('is_revisor', NULL)->get();
        $writerRequests = User::where('is_writer', NULL)->get();
        $tags = Tag::all();
        $categories = Category::all();
        return view('adminDashboard', compact('adminRequests', 'revisorRequests', 'writerRequests', 'tags'));
    }

    public function makeUserAdmin(User $user)
    {
        $user->is_admin = true;
        $user->save();
        return redirect()->route('adminDashboard');
    }

    public function makeUserRevisor(User $user)
    {
        $user->is_revisor = true;
        $user->save();
        return redirect()->route('adminDashboard');
    }

    public function makeUserWriter(User $user)
    {
        $user->is_writer = true;
        $user->save();
        return redirect()->route('adminDashboard');
    }

    public function editTag(Request $request, Tag $tag)
    {
        $tag->update(
            [
                'name' => $request->input('name')
            ]
        );
        return redirect()->route('adminDashboard');
    }

    public function deleteTag(Tag $tag)
    {
        $tag->delete();
        return redirect()->route('adminDashboard');
    }

    public function storeTag(Request $request)
    {
        Tag::create(
            [
                'name' => $request->input('name')
            ]
        );
        return redirect()->route('adminDashboard');
    }

    public function editCategory(Request $request, Category $category)
    {
        $category->update(
            [
                'name' => $request->input('name')
            ]
        );
        return redirect()->route('adminDashboard');
    }

    public function deleteCategory(Category $category)
    {
        $category->delete();
        return redirect()->route('adminDashboard');
    }

    public function storeCategory(Request $request)
    {
        $test = Category::create(['name' => $request->input('name')]);
        return redirect()->route('adminDashboard');
    }
}
